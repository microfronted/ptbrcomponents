import { IGetFormatedCpfCnpjParams } from "./interfaces";

export const formatsFields = {
  CPF: "###.###.###-##",
  CNPJ: "##.###.###/####-##",
};

export const getOnlyNumbers = (value = ""): string =>
  value.replace(/[^0-9]/g, "");

export const getFormatedCpfCnpjValue = ({
  formatedType,
  cpfCnpj,
  mask = "_",
}: IGetFormatedCpfCnpjParams): string => {
  let numberCount = 0;
  return formatsFields[formatedType]
    .split("")
    .reduce((formatedValue: string, current: string) => {
      let value = current;
      if (current === "#") {
        value = cpfCnpj[numberCount] || mask;
        numberCount++;
      }
      return `${formatedValue}${value}`;
    }, "");
};
