import { TextFieldProps } from "@mui/material";
import { NumberFormatProps } from "react-number-format";

export type CpfCnpjProps = Partial<NumberFormatProps & TextFieldProps> & {};

export interface IGetFormatedCpfCnpjParams {
  formatedType: string;
  cpfCnpj: string;
  mask: string;
}
