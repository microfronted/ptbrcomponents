import React, { useState } from "react";
import NumberFormat from "react-number-format";
import { cnpj, cpf } from "cpf-cnpj-validator";
import { getFormatedCpfCnpjValue, getOnlyNumbers } from "./helpers";
import { TextField } from "@mui/material";
import { CpfCnpjProps } from "./interfaces";

const FieldCpfCnpj = ({ mask = "_" }: CpfCnpjProps) => {
  const [formatType, setFormatType] = useState("CPF");
  // const [customerId, setCustomerId] = useState<string | null>(null);
  // const [isValidClient, setIsValidClient] = useState<boolean>(false);

  const handleFormat = (nextValue: string): string => {
    const nextFormatType = nextValue.length <= 11 ? "CPF" : "CNPJ";

    if (formatType !== nextFormatType) {
      setFormatType(nextFormatType);
    }

    return getFormatedCpfCnpjValue({
      formatedType: nextFormatType,
      cpfCnpj: nextValue,
      mask: String(mask),
    });
  };

  const handleOnBlur = (
    e: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement, Element>
  ) => {
    const customerIdNumbers = getOnlyNumbers(e.target.value) || "";
    const isValidCustomer =
      formatType === "CPF"
        ? cpf.isValid(customerIdNumbers)
        : cnpj.isValid(customerIdNumbers);

    console.log({
      isValidCustomer,
      customerIdNumbers,
    });

    // TODO: send to another microservice
    // setCustomerId(customerIdNumbers);
    // setIsValidClient(isValidCustomer);
  };

  return (
    <NumberFormat
      customInput={TextField}
      mask={mask}
      format={handleFormat}
      onBlur={handleOnBlur}
      fullWidth={true}
      margin="none"
    />
  );
};

export default FieldCpfCnpj;
