import React from "react";
import ReactDOM from "react-dom";

import "./index.css";

import FieldCpfCnpj from "fieldCpfCnpj/CpfCnpj";

const App = () => (
  <div className="container">
    <div>Name: home</div>
    <div>Framework: react</div>
    <div>Language: TypeScript</div>
    <div>CSS: Empty CSS</div>
    <FieldCpfCnpj />
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
